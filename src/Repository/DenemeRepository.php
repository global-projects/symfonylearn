<?php

namespace App\Repository;

use App\Entity\Deneme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Deneme|null find($id, $lockMode = null, $lockVersion = null)
 * @method Deneme|null findOneBy(array $criteria, array $orderBy = null)
 * @method Deneme[]    findAll()
 * @method Deneme[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DenemeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Deneme::class);
    }

    // /**
    //  * @return Deneme[] Returns an array of Deneme objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Deneme
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
