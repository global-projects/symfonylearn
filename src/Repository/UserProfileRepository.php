<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserProfile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserProfile|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserProfile|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserProfile[]    findAll()
 * @method UserProfile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserProfileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserProfile::class);
    }

    public function getUserData()
    {
        $getData = $this->createQueryBuilder("up")
            ->select("up.name","up.surname","up.gsm")
            ->where("up.user=:user")
            ->setParameter("user",1)
            ->getQuery()
            ->getArrayResult();

        return $getData;
    }

    public function newUserProfile(int $user)
    {
        $em = $this->getEntityManager();

        $newUserProfile = new UserProfile();
        $newUserProfile
            ->setName("ali")
            ->setSurname("sunal")
            ->setGsm("05556667788")
            ->setUser($em->find(User::class,$user));
        $em->persist($newUserProfile);
        $em->flush();

        return $newUserProfile;
    }
}
