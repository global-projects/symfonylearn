<?php

namespace App\Repository;

use App\Entity\Docker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Docker|null find($id, $lockMode = null, $lockVersion = null)
 * @method Docker|null findOneBy(array $criteria, array $orderBy = null)
 * @method Docker[]    findAll()
 * @method Docker[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DockerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Docker::class);
    }

    // /**
    //  * @return Docker[] Returns an array of Docker objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Docker
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
