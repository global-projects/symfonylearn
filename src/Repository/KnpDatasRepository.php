<?php

namespace App\Repository;

use App\Entity\KnpDatas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method KnpDatas|null find($id, $lockMode = null, $lockVersion = null)
 * @method KnpDatas|null findOneBy(array $criteria, array $orderBy = null)
 * @method KnpDatas[]    findAll()
 * @method KnpDatas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KnpDatasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KnpDatas::class);
    }

    /**
     * @return int|mixed[]|string
     */
    public function getKnpDatas(Request $request,array $postData,PaginatorInterface $paginator)
    {
        $result = ["success" => false, "message" => "İşlem yapılamadı","data"=>[]];
        try {
            $getKnpData = $this->createQueryBuilder("k")
                ->select("k.name","k.id")
                ->getQuery();

            $page = $request->query->getInt('page', 1);


            $paging = $paginator->paginate(
                $getKnpData,
                $page,
                isset($postData["pageItemLimit"]) ? $postData["pageItemLimit"] : 999999999
            );

            $result["success"] = true;
            $result["message"] = "Successfully";
            $result["data"] = $paging;

        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
        }

        return $result;
    }



    /**
     * @return int|mixed[]|string
     */
    public function getDataTable()
    {
        $result = ["success" => false, "message" => "İşlem yapılamadı","data"=>[]];
        try {
            $paging = $this->createQueryBuilder("k")
                ->select("k.name","k.id")
                ->getQuery()->getArrayResult();


            $result["success"] = true;
            $result["message"] = "Successfully";
            $result["data"] = $paging;

        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
        }

        return $result;
    }
}
