<?php

namespace App\Repository;

use App\Entity\Book;
use App\Entity\BookLike;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    // /**
    //  * @return Book[] Returns an array of Book objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Book
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    /**
     * @return array
     */
    public function getBookWithSubQuery()
    {
        $result = ["success" => false, "message" => "İşlem yapılamadı", "data" => []];
        try {

            $getBook = $this->createQueryBuilder("b")
                ->select("b.name", "b.price", "b.quantity")
                ->addSelect("(SELECT SUM(b1.quantity) FROM " . Book::class . " b1) as sumQuantity")
                ->getQuery()
                ->getArrayResult();

            $result["success"] = true;
            $result["message"] = "Silme Başarılı";
            $result["data"] = $getBook;

        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
        }

        return $result;
    }

    public function getBookWithCategory(array $postData)
    {
        $result = ["success" => false, "message" => "No action taken", "data" => []];
        try {
            $getBook = $this->createQueryBuilder("b");
            $getBook
                ->select("b.name as bookName")
                ->addSelect("b.id")
                ->addSelect("c.name as categoryName");
            if ($postData["user"] != null) {
                $getBook
                    ->addSelect("(SELECT bl1.id FROM " . BookLike::class . " bl1 WHERE bl1.book=b.id AND bl1.user=" . $postData["user"] . ") as blLike");
            }
            $getBook
                ->addSelect("(SELECT COUNT(bl2.id) FROM " . BookLike::class . " bl2 WHERE bl2.book=b.id ) as bookLikeCount")
                ->leftJoin("b.category", "c")
                ->where("b.category=:catId")
                ->setParameter("catId", $postData["id"]);
            $getBook = $getBook->getQuery()->getArrayResult();
            $result["success"] = true;
            $result["message"] = "Successfully";
            $result["data"] = $getBook;

        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
        }
        return $result;
    }


    public function newBookManyToOne(int $catId)
    {
        $em = $this->getEntityManager();

        $newBook = new Book();
        $newBook
            ->setName("Oyun")
            ->setPrice(22)
            ->setQuantity(3)
            ->setCategory($em->find(Category::class, $catId));
        $em->persist($newBook);
        $em->flush();

        return $newBook;
    }

    public function newBook(array $postData)
    {
        $result = ["success" => false, "message" => "No action taken"];
        try {
            $em = $this->getEntityManager();

            $newBook = new Book();
            $newBook
                ->setName($postData["name"])
                ->setPrice($postData["price"])
                ->setQuantity($postData["quantity"])
                ->setBrochureFilename($postData["brochureFilename"])
                ->setCategory($em->find(Category::class, (int)$postData["category"]));
            $em->persist($newBook);
            $em->flush();
            $result["success"] = true;
            $result["message"] = "Successfully";

        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
        }
        return $result;
    }


    /**
     * @param array $postData
     * @return array
     */
    public function updateBook(array $postData)
    {
        $result = ["success" => false, "message" => "İşlem yapılamadı"];
        try {
            $em = $this->getEntityManager();
            $updateBook = $em->find(Book::class, $postData["id"]);
            $updateBook
                ->setName($postData["name"])
                ->setPrice($postData["price"])
                ->setQuantity($postData["quantity"])
                ->setCategory($em->find(Category::class, (int)$postData["category"]));

            $em->persist($updateBook);
            $em->flush();

            $result["success"] = true;
            $result["message"] = "Başarılı";
        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
        }

        return $result;
    }


    /**
     * @param array $postData
     * @return array
     */
    public function getSingleBook(array $postData)
    {
        $result = ["success" => false, "message" => "İşlem yapılamadı", "data" => []];
        try {

            $getBook = $this->createQueryBuilder("b")
                ->select("b.name", "b.price", "b.quantity")
                ->where("b.id=:bId")
                ->setParameter("bId", $postData["id"])
                ->getQuery()
                ->getOneOrNullResult();

            $result["success"] = true;
            $result["message"] = "SuccessFully";
            $result["data"] = $getBook;

        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
        }

        return $result;
    }
}
