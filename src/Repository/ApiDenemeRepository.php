<?php

namespace App\Repository;

use App\Entity\ApiDeneme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApiDeneme|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiDeneme|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiDeneme[]    findAll()
 * @method ApiDeneme[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApiDenemeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApiDeneme::class);
    }

    // /**
    //  * @return ApiDeneme[] Returns an array of ApiDeneme objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ApiDeneme
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
