<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TwigFilterController extends AbstractController
{
    /**
     * @Route("/twig-filter", name="twig_filter")
     */
    public function index(): Response
    {
        $date = new \DateTime();
        $arrData =[
            "data1"=>1,
            "data2"=>2,
            "data3"=>3,
            "data4"=>4,
            "data5"=>5,
        ];
        return $this->render('twig_filter/index.html.twig', [
            'controller_name' => 'TwigFilterController',
            'nowDate' => $date,
            'arrData' => $arrData,
        ]);
    }
}
