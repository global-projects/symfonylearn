<?php

namespace App\Controller;

use App\Entity\KnpDatas;
use App\Repository\KnpDatasRepository;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Adapter\ArrayAdapter;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Adapter\Elasticsearch\ElasticaAdapter;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Column\MapColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\DataTable;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OnimesDataTableController extends AbstractController
{


    private $factory;

    public function __construct(
        DataTableFactory $factory
    ) {
        $this->factory = $factory;
    }

    /**
     * Creates and returns a basic DataTable instance.
     *
     * @param array $options Options to be passed
     * @return DataTable
     */
    protected function createDataTable(array $options = [])
    {
        return $this->factory->create($options);
    }


    /**
     * Creates and returns a DataTable based upon a registered DataTableType or an FQCN.
     *
     * @param string $type FQCN or service name
     * @param array $typeOptions Type-specific options to be considered
     * @param array $options Options to be passed
     * @return DataTable
     */
    protected function createDataTableFromType($type, array $typeOptions = [], array $options = [])
    {
        return $this->factory->createFromType($type, $typeOptions, $options);
    }

    /**
     * @Route("/onimes-create", name="onimes_data_table_create")
     */
    public function create(Request $request, DataTableFactory $dataTableFactory,KnpDatasRepository $knpDatasRepository)
    {
        $getData = $knpDatasRepository->getDataTable()["data"];
//        $getData=array_column($getData,)
//        dump($getData);
//        exit();

        $table = $dataTableFactory->create()
            ->add('id', TextColumn::class)
            ->add('name', TextColumn::class)
            ->createAdapter(ArrayAdapter::class, $getData)
            ->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('onimes_data_table/index.html.twig', ['datatable' => $table]);
    }
    /**
     * @Route("/onimes", name="onimes_data_table")
     */
    public function showAction(Request $request, DataTableFactory $dataTableFactory)
    {
           $table = $this->createDataTable()
               ->add('id', TextColumn::class)
               ->add('name', TextColumn::class)
               ->createAdapter(ORMAdapter::class, [
                   'entity' => KnpDatas::class,
               ])
            ->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('onimes_data_table/index.html.twig', ['datatable' => $table]);
    }
//    /**
//     * @Route("/customizing", name="customizing")
//     */
//    public function customizing(Request $request, DataTableFactory $dataTableFactory)
//    {
//        $table=$this->createAdapter(ORMAdapter::class, [
//            'entity' => KnpDatas::class,
//            'query' => function (QueryBuilder $builder) {
//                $builder
//                    ->select('e')
////                    ->addSelect('c')
//                    ->from(KnpDatas::class, 'e')
////                    ->leftJoin('e.company', 'c')
//                ;
//            },
//        ])
//            ->handleRequest($request);
//
//        if ($table->isCallback()) {
//            return $table->getResponse();
//        }
//
//        return $this->render('onimes_data_table/index.html.twig', ['datatable' => $table]);
//    }

//    /**
//     * @Route("/elastica", name="elastica")
//     */
//    public function elastica(Request $request, DataTableFactory $dataTableFactory)
//    {
//        $table = $this->createDataTable()
//            ->setName('log')
//            ->add('timestamp', DateTimeColumn::class, ['field' => '@timestamp', 'format' => 'Y-m-d H:i:s', 'orderable' => true])
//            ->add('level', MapColumn::class, [
//                'default' => '<span class="label label-default">Unknown</span>',
//                'map' => ['Emergency', 'Alert', 'Critical', 'Error', 'Warning', 'Notice', 'Info', 'Debug'],
//            ])
//            ->add('message', TextColumn::class, ['globalSearchable' => true])
//            ->createAdapter(ElasticaAdapter::class, [
//                'client' => ['host' => 'elasticsearch'],
//                'index' => 'logstash-*',
//            ])
//            ->handleRequest($request);
//
//        if ($table->isCallback()) {
//            return $table->getResponse();
//        }
//
//        return $this->render('onimes_data_table/index.html.twig', ['datatable' => $table]);
//    }
}
