<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function PHPUnit\Framework\exactly;

class TemplateDesignController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        $allCategory = $categoryRepository->getCategory()["data"];

        return $this->render('template_design/index.html.twig', [
            'allCategory' =>$allCategory,
        ]);
    }
}
