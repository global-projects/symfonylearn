<?php

namespace App\Controller;

use App\Entity\KnpDatas;
use App\Form\KnpDatasType;
use App\Repository\CategoryRepository;
use App\Repository\KnpDatasRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/knp-datas")
 */
class KnpDatasController extends AbstractController
{


    /**
     * @Route("/", name="knp_datas_index", methods={"GET"})
     */
    public function index(Request $request,KnpDatasRepository $knpDatasRepository,PaginatorInterface $paginator): Response
    {
        $postData = $request->request->all();
        $postData["pageItemLimit"] = 100;
        $getData = $knpDatasRepository->getKnpDatas($request,$postData,$paginator)["data"];

        return $this->render('knp_datas/index.html.twig', [
            'knp_datas' => $getData,
        ]);
    }


    /**
     * @Route("/table", name="knp_datas_index2", methods={"GET"})
     */
    public function index2(KnpDatasRepository $knpDatasRepository): Response
    {
        $getData = $knpDatasRepository->getDataTable()["data"];
        return $this->render('knp_datas/index2.html.twig', [
            'knp_datas' => $getData,
        ]);
    }

    /**
     * @Route("/new", name="knp_datas_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $knpData = new KnpDatas();
        $form = $this->createForm(KnpDatasType::class, $knpData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($knpData);
            $entityManager->flush();

            return $this->redirectToRoute('knp_datas_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('knp_datas/new.html.twig', [
            'knp_data' => $knpData,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="knp_datas_show", methods={"GET"})
     */
    public function show(KnpDatas $knpData): Response
    {
        return $this->render('knp_datas/show.html.twig', [
            'knp_data' => $knpData,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="knp_datas_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, KnpDatas $knpData, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(KnpDatasType::class, $knpData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('knp_datas_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('knp_datas/edit.html.twig', [
            'knp_data' => $knpData,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="knp_datas_delete", methods={"POST"})
     */
    public function delete(Request $request, KnpDatas $knpData, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$knpData->getId(), $request->request->get('_token'))) {
            $entityManager->remove($knpData);
            $entityManager->flush();
        }

        return $this->redirectToRoute('knp_datas_index', [], Response::HTTP_SEE_OTHER);
    }
}
