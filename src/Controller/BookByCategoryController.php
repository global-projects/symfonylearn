<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class BookByCategoryController extends AbstractController
{
    /**
     * @Route("/book-by-category/{category}", name="book_by_category")
     * @param Request $request
     * @param Category|null $category
     * @param BookRepository $bookRepository
     * @param UserInterface|null $actUser
     * @return Response
     */
    public function index(Request $request, Category $category=null, BookRepository $bookRepository,UserInterface $actUser=null): Response
    {

        $postData=$request->query->all();
        $postData["user"] = null !==$actUser ? $actUser->getId() : null;
        $getbooks = $bookRepository->getBookWithCategory($postData)["data"];

        return $this->render('book_by_category/index.html.twig', [
            'getbooks' => $getbooks,
        ]);
    }
}
