<?php

namespace App\Controller;

use App\Entity\Book;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SingleBookController extends AbstractController
{
    /**
     * @Route("/single-book/{book}", name="single_book")
     */
    public function index(Request $request, Book $book=null, BookRepository $bookRepository): Response
    {
        $postData = array_merge($request->query->all());
        $getSingleBook = $bookRepository->getSingleBook($postData)["data"];
        return $this->render('single_book/index.html.twig', [
            'getSingleBook' => $getSingleBook,
        ]);
    }
}
