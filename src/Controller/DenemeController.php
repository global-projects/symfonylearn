<?php

namespace App\Controller;

use App\Entity\Team;
use App\Entity\User;
use App\Form\BookType;
use App\Form\DenemeType;
use App\Form\FirstType;
use App\Repository\BookRepository;
use App\Repository\CategoryRepository;
use App\Repository\TeamRepository;
use App\Repository\UserProfileRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class DenemeController extends AbstractController
{
    /**
     * @Route("/deneme", name="deneme_show")
     */
    public function index(Request $request): Response
    {
        $server = $request->server->get("REMOTE_ADDR");

        return new Response($server);

        dump($server);
        dump($request);
        exit();
    }

    /**
     * @Route("/book", name="book")
     */
    public function book(BookRepository $bookRepository): Response
    {
        $getBook = $bookRepository->getBookWithSubQuery();
        dump($getBook["data"]);
        exit();
        return $this->render('book/index.html.twig', [
            'controller_name' => 'BookController',
        ]);
    }


    /**
     * @Route("/category", name="category")
     */
    public function category(CategoryRepository $categoryRepository): Response
    {
//        $getCategory = $categoryRepository->getCategory();
        $find = $categoryRepository->find(2);
        $findAll = $categoryRepository->findAll();
        $findBy = $categoryRepository->findBy(["name" => "Hikaye"]);
        $findOneBy = $categoryRepository->findBy(["name" => "Hikaye"]);
        dump($find);
        dump($findAll);
        dump($findBy);
        dump($findOneBy);
        exit();
        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
        ]);
    }

    /**
     * @Route ("/new-category", name="new_category")
     * @param CategoryRepository $categoryRepository
     */
    public function newCategory(CategoryRepository $categoryRepository)
    {
        $newCategory = $categoryRepository->newCategory();
        dump($newCategory);
        exit();
    }

    /**
     * @Route ("/get-single-show-category/{id}", name="get_single_show_category")
     * @param CategoryRepository $categoryRepository
     * @param $id
     */
    public function getSingleShowCategory(CategoryRepository $categoryRepository, $id)
    {
        $getSingleShowCategory = $categoryRepository->getShowSingleCategory($id);
        dump($getSingleShowCategory["data"]);
        exit();
    }


    /**
     * @Route ("/update-category/{id}" ,name="update_category")
     * @param CategoryRepository $categoryRepository
     * @param $id
     */
    public function updateCategory(CategoryRepository $categoryRepository, $id)
    {
        $postData["setName"] = "Masal";
        $updateCategory = $categoryRepository->updateCategory($id, $postData);
        dump($updateCategory);
        exit();
    }


    /**
     * @Route ("/delete-category/{id}" ,name="delete_category")
     * @param CategoryRepository $categoryRepository
     * @param $id
     */
    public function deleteCategory(CategoryRepository $categoryRepository, $id)
    {
        $delete = $categoryRepository->deleteCategory($id);
        exit();
    }

    /**
     * @Route ("/raw-sql", name="raw_sql")
     */
    public function rawSql()
    {
        $em = $this->getDoctrine()->getManager();
        $sql = "SELECT * FROM category LIMIT 3";
        $stmt = $em->getConnection()->executeQuery($sql)->fetchAllAssociative();
        dump($stmt);
        exit();
    }

    /**
     * @Route ("/get-many-to-one", name="many_to_one")
     */
    public function getBookWithCat(CategoryRepository $categoryRepository, BookRepository $bookRepository)
    {
//        $getData = $categoryRepository->getBookWithCategory(8);
        $getData = $bookRepository->getBookWithCategory(8);
        dump($getData);
        exit();
    }

    /**
     * @Route("/team", name="team")
     */
    public function team(TeamRepository $teamRepository): Response
    {

        $getData = $teamRepository->getTeamUser(2);
        dump($getData);
        exit();

        return $this->render('team/index.html.twig', [
            'controller_name' => 'TeamController',
        ]);
    }

    /**
     * @Route("/user-profile", name="user_profile")
     */
    public function profile(UserProfileRepository $userProfileRepository): Response
    {

        $getData = $userProfileRepository->getUserData();
        dump($getData);
        exit();
        return $this->render('user_profile/index.html.twig', [
            'controller_name' => 'UserProfileController',
        ]);
    }

    /**
     * @Route("/set-new-cat-book", name="set_new_cat_book")
     */
    public function newCatAndBook(CategoryRepository $categoryRepository, BookRepository $bookRepository)
    {
        $setCat = $categoryRepository->newCategoryManyToOne();
        $setBook = $bookRepository->newBookManyToOne($setCat->getId());
        dump("katdedildi");
        exit();
    }

    /**
     * @Route("/set-many-to-many", name="set_many_to_many")
     */
    public function newManyToMany()
    {
        $em = $this->getDoctrine()->getManager();

        $user = new User();
        $user
            ->setEmail("deneme@gmail.com")
            ->setEmailCanonical("deneme@gmail.com")
            ->setUsername("deneme@gmail.com")
            ->setUsernameCanonical("deneme@gmail.com")
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword(123123123)
            ->setEnabled(true);

        $team = new Team();

        $team
            ->setName("ManyToMany");

        $team->addUser($user);

        $em->persist($user);
        $em->persist($team);
        $em->flush();
        dump("kaydedildi");
        exit();
    }

    /**
     * @Route("/set-one-to-one", name="set_one_to_one")
     */
    public function newOneToOne(UserProfileRepository $userProfileRepository)
    {
        $em = $this->getDoctrine()->getManager();

        $user = new User();
        $user
            ->setEmail("deneme11@gmail.com")
            ->setEmailCanonical("deneme11@gmail.com")
            ->setUsername("deneme11@gmail.com")
            ->setUsernameCanonical("deneme11@gmail.com")
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword(123123123)
            ->setEnabled(true);
        $em->persist($user);
        $em->flush();
        $up = $userProfileRepository->newUserProfile($user->getId());

        $em->persist($up);
        $em->flush();
        dump("2 dosya kaydedildi");
        exit();
    }

    /**
     * @Route ("/first-form")
     * @return Response
     */
    public function firstForm()
    {
        $form = $this->createForm(DenemeType::class);

        return $this->render('deneme/index2.html.twig', [
            "form" => $form->createView()
        ]);
    }
//    /**
//     * @Route ("/book-form")
//     * @return Response
//     */
//    public function bookFirst()
//    {
//        $form = $this->createForm(BookType::class);
//
//        return $this->render('deneme/index.html.twig', [
//            "form"=>$form->createView()
//        ]);
//    }

    /**
     * @Route("/api-deneme", name="api_deneme")
     * @param CategoryRepository $categoryRepository
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function apiDeneme(CategoryRepository $categoryRepository)
    {

        $getData = $categoryRepository->getCategory()["data"];

        return $this->json($getData);
    }
}
