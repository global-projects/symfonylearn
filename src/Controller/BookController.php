<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\BookLike;
use App\Form\BookType;
use App\Repository\BookLikeRepository;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/book")
 */
class BookController extends AbstractController
{
    /**
     * @Route("/", name="book_index", methods={"GET"})
     */
    public function index(BookRepository $bookRepository): Response
    {
        return $this->render('book/index.html.twig', [
            'books' => $bookRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="book_new", methods={"GET","POST"})
     */
    public function new(Request $request, BookRepository  $bookRepository): Response
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $postData = $request->request->all();

            $brochureFile = $form->get('brochure')->getData();

            $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);

            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);

            $newFilename = $safeFilename.'-'.uniqid().'.'.$brochureFile->guessExtension();

            try {
                $brochureFile->move(
                    $this->getParameter('brochures_directory'),
                    $newFilename
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }

            $postData["book"]["brochureFilename"]= $newFilename;

            $newBook = $bookRepository->newBook($postData["book"]);
            return $this->redirectToRoute('book_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('book/new.html.twig', [
            'book' => $book,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="book_show", methods={"GET"})
     */
    public function show(Book $book): Response
    {
        return $this->render('book/show.html.twig', [
            'book' => $book,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="book_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Book $book, BookRepository $bookRepository): Response
    {
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $postData = $request->request->all();
            $updateBook = $bookRepository->updateBook($postData["book"]);
            return $this->redirectToRoute('book_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('book/edit.html.twig', [
            'book' => $book,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="book_delete", methods={"POST"})
     */
    public function delete(Request $request, Book $book): Response
    {
        if ($this->isCsrfTokenValid('delete'.$book->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($book);
            $entityManager->flush();
        }

        return $this->redirectToRoute('book_index', [], Response::HTTP_SEE_OTHER);
    }


    /**
     * @Route("/{id}/like", name="book_like")
     * @param Book $book
     * @param BookLikeRepository $bookLikeRepository
     * @return Response
     */
    public function like(Book $book,BookLikeRepository $bookLikeRepository) : Response
    {
        $manager = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if (!$user) return $this->json([
            "code"=>403,
            "message"=>"No AUTH",
        ],403);

        if ($book->bookLike($user)){
            $like = $bookLikeRepository->findOneBy([
                "book"=>$book,
                "user"=>$user,
            ]);

            $manager->remove($like);
            $manager->flush();

            return $this->json([
                "code"=>200,
                "message"=>"success",
                "likes"=>$bookLikeRepository->count(["book"=>$book])
            ],200);
        }


        $like = new BookLike();
        $like
            ->setBook($book)
            ->setUser($user);

        $manager->persist($like);
        $manager->flush();

        return $this->json([
            "code"=>200,
            "message"=>"like",
            "likes"=>$bookLikeRepository->count(["book"=>$book])
        ],200);

    }
}
